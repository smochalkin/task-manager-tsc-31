package ru.smochalkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.repository.ITaskRepository;
import ru.smochalkin.tm.api.service.ITaskService;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyNameException;
import ru.smochalkin.tm.model.Task;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public final class TaskService extends AbstractBusinessService<Task> implements ITaskService {

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
    }

    @Override
    @NotNull
    public Task create(@NotNull final String userId, @Nullable final String name, @Nullable final String description) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        businessRepository.add(task);
        return task;
    }

}
