package ru.smochalkin.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.data.fasterxml.BackupLoadCommand;
import ru.smochalkin.tm.command.data.fasterxml.BackupSaveCommand;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class Backup {

    private final int interval = 30;

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    public final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        load();
        es.scheduleWithFixedDelay(this::save, interval, interval, TimeUnit.SECONDS);
    }

    public void save() {
        bootstrap.parseCommand(BackupSaveCommand.NAME);
    }

    public void load() {
        bootstrap.parseCommand(BackupLoadCommand.NAME);
    }

}


