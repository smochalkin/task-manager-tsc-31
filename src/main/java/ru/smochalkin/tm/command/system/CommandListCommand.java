package ru.smochalkin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractSystemCommand;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;

import java.util.List;

public final class CommandListCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String arg() {
        return "-cmd";
    }

    @Override
    @NotNull
    public String name() {
        return "commands";
    }

    @Override
    @NotNull
    public String description() {
        return "Display list of commands.";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        if (serviceLocator == null) throw new EmptyObjectException();
        @NotNull final List<String> keys = serviceLocator.getCommandService().getCommandNames();
        for (String value : keys) {
            if (value == null || value.isEmpty()) continue;
            System.out.println(value);
        }
    }

}
